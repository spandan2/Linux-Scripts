# ArchLinux UEFI x86_64
## You are here so I assume you are cool.( or at least trying to be ;) )  
#### A. Please ensure that UEFI is enabled on your motherboard

`# ls /sys/firmware/efi/efivars`

if you see a list of files returning then your system is UEFI enabled and it is safe for you to continue with these steps



###### General Installation procedure (standard install on EFI):
  1. Use wifi-menu to connect to network
  1.1. Ping a server to check `# ping www.google.com`
  2. Start ssh `# systemctl start sshd` (if needed)
  3. Connect to machine via SSH
  4. Visit https://www.archlinux.org/mirrorlist/ on another computer, generate mirrorlist<br>
  5. 
     *  Edit /etc/pacman.d/mirrorlist on the Arch computer and paste the faster servers <br>
     *  Or use Reflector to do so <br>
     `# sudo pacman -Sy reflector`<br>
     `# reflector -c 'India' -f 12 -l 12 -n 12 --verbose --save /etc/pacman.d/mirrorlist` - Reflector already has curated mirrors for your country<br>
     *  Or you can sort the mirrorlist by pinging the servers<br>
     First create a backup of the mirrorlist and then use rankmirrors to find n best mirrors<br>
     `# cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup`<br>
     `# rankmirrors -n 10 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist`<br>

  6. Update package indexes: `# pacman -Syy`
  7. 
  A.    Using fdisk<br>
  Create efi partition:(stick to this one for fdisk)

       `# fdisk /dev/sda`

          * g (to create an empty GPT partition table)
          * n
          * 1
          * enter
          * +300M
          * t
          * 1 (For EFI)
          * w

  8. Create root partition:

      `# fdisk /dev/sda`

         * n
         * 2
         * enter
         * +30G
         * w

  9. Create home partition:

       `# fdisk /dev/sda`

          * n
          * 3
          * enter
          * enter
          * w
#### Alternately use # `# cfdisk /dev/sda`  to create something like <br>
![cfdisk](https://gitlab.com/spandan2/Linux-Scripts/raw/master/Arch/cfdisk.png)<br>

  10. `# mkfs.fat -F32 /dev/sda1`
  11. `# mkfs.ext4 /dev/sda2`
      `# mkfs.ext4 /dev/sda3`
      `# mount /dev/sda2 /mnt`
  12. `# mkdir /mnt/home`
  13. `# mount /dev/sda3 /mnt/home`
  14. `# pacstrap -i /mnt base`
  15. `# genfstab -U -p /mnt >> /mnt/etc/fstab`
  16. `# arch-chroot /mnt`
  17. `# pacman -S base-devel grub efibootmgr dosfstools ntfs-3g openssh os-prober mtools linux-headers linux-lts linux-lts-headers` - boot related <br>
  18. `# nano /etc/locale.gen` (uncomment en_US.UTF-8)
  19. `# locale-gen`
  20. Enable `root` logon via `ssh`
  21. `# systemctl enable sshd.service`
  22. `# passwd` (for setting root password)
  23. `# mkdir /boot/EFI`
  24. `# mount /dev/sda1 /boot/EFI`
  25. `# pacman -S dialog networkmanager wpa_supplicant wireless_tools network-manager-applet nm-connection-editor links` - internet & WiFi
  26. `# grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck`
  27. `# cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo`
  28. `# grub-mkconfig -o /boot/grub/grub.cfg`
  29. Create swap file:
        * `# fallocate -l 2G /swapfile`
        * `# chmod 600 /swapfile`
        * `# mkswap /swapfile`
        * `# echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab` - or you can manually edit the /etc/fstab file using your text editor
  30. `$ exit`
  31. `# umount -a`
  32. `# reboot`

## Post installation (GUI and stuff)
1. `$ sudo su`
2. `# pacman -S xorg xorg-apps`
3. `# pacman -S lightdm lightdm-gtk-greeter`
4. `# systemctl enable NetworkManager.service`
5. `# systemctl enable lightdm.service`
6. `# pacman -S pulseaudio pulseaudio-alsa alsa`
7. `# pacman -S xfce4 plasma-desktop deepin deepin-extra`
8. `# pacman -S xfce4-pulseaudio-plugin`
9. For AUR .... <br>
* `# nano /etc/pacman.conf`
* Uncomment multilib
* Add the entry <br>
```
[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/$arch
```
* Save
10. `# pacman -Syyu`
11. `# pacman -S nvidia-dkms`
12. Reboot


